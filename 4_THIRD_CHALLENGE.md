

# Third Challenge
Congratulations ⚔️, you're now the hero of the Customer Success Team and other developer
because of your excellent testing chops. 🙌

The dashboard is function just fine but with so many people applying for funds, it's getting a little slow to load all of the records in each time. The Customer Success Manager comes to you and asks if there's anything that can be done to speed it up. You both decide that adding pagination to the table/API should do the trick.

### Bonus modifications:
- `material-ui` also supports sorting within their library, add sorting
to the table as well so that you can sort by students without payments so there's even less scrolling/page changes.
- Make a proposal: there are a million ways to improve a codebase. Propose a change that could be made to the application that would make it better for either the developer or the "customer" and explain the impact that it would have.

# Proposals
1. Separate users and applications into separate parts of the app. Then we wouldn't need to filter based on if a user has an application
2. Better visual indicator of if an application has been paid out
3. Hide UUIDs from user as that's likely information only useful to a developer (unless that's how applications are publicly identified)
4. I am an Angular developer (so I am not famliliar with React), but the architecture does not seem ideal. Surely there is a better way to organize the view and the data.
5. While this is a small application meant as an exercise, there seems to be no error handling--which would be needed in a production app