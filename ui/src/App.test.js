import { render, waitFor, screen } from "@testing-library/react";
import App from "./App";

const HAS_REQUESTED_AMOUNT = 'requestedAmount';
const HAS_PAYMENT_AMOUNT = 'paymentAmount';
const HAS_PAYMENT_METHOD = 'paymentMethod';
const INITIATE_PAYMENT_BUTTON_EXISTS = 'initiatePaymentButtonExists';

test("table renders with headers", async () => {
  render(<App />);
  await waitFor(() => screen.getByRole("table"));
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

// Test payment button shows if requested amount exists and not yet paid
test("payment button shows if requested amount exists and not yet paid", async () => {
  const rows = await getRows();
  rows.forEach((row) => {
    if (row.HAS_REQUESTED_AMOUNT && !row.HAS_PAYMENT_AMOUNT && !row.HAS_PAYMENT_METHOD) {
      expect(row.INITIATE_PAYMENT_BUTTON_EXISTS).toEqual(true);
    }
  });
});

// Test payment button is hidden if request amount does not exist
test("payment button hidden if requested amount does not exist", async () => {
  const rows = await getRows();
  rows.forEach((row) => {
    if (!row.HAS_REQUESTED_AMOUNT) {
      expect(row.INITIATE_PAYMENT_BUTTON_EXISTS).toEqual(false);
    }
  });
});

// Test payment button is hidden if payment has occured
test("payment button hidden if payment has occured", async () => {
  const rows = await getRows();
  rows.forEach((row) => {
    if (row.HAS_PAYMENT_AMOUNT && row.HAS_PAYMENT_METHOD) {
      expect(row.INITIATE_PAYMENT_BUTTON_EXISTS).toEqual(false);
    }
  });
});

let getRows = async () => {
  render(<App />);
  await waitFor(() => screen.getByRole("table"));
  let rows = screen.getAllByRole("row");
  // This removes the header row
  rows.shift();
  // This removes the pagination row
  rows.pop();
  return rows.map(row => { return {
    HAS_REQUESTED_AMOUNT: row.children[3].textContent,
    HAS_PAYMENT_AMOUNT: row.children[4].textContent,
    HAS_PAYMENT_METHOD: row.children[5].textContent,
    INITIATE_PAYMENT_BUTTON_EXISTS: !!row.children[6].textContent,
  }});
};
