import React, { useState, useEffect } from "react";
import "./App.css";
import Table from "./components/table.jsx";
import { Container, Button } from "@material-ui/core";
import formatCurrency from "./utils/formatCurrency";

import { getUserCount, getUsers } from "./services/users.js";
import { getApplications } from "./services/applications.js";
import { getPayments, createPayment } from "./services/payments.js";

const App = () => {
  /**
   * Hydrate data for the table and set state for users, applications, and payments
   */
  const [userCount, setUserCount] = useState(0);
  const [payments, setPayments] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  const [paginationPage, setPaginationPage] = useState(0);
  const [paginationCount, setPaginationCount] = useState(10);
  const [tableData, setTableData] = useState([]);

  async function fetchData(from, count) {
    const [usersData, userCountData, applicationsData, paymentsData] =
      await Promise.all([
        getUsers(from, count),
        getUserCount(),
        getApplications(),
        getPayments(),
      ]);

    setUserCount(userCountData.body);
    setPayments(paymentsData.body);
    setTableData(
      usersData.body.map(({ uuid, name, email }) => {
        const { requestedAmount, uuid: applicationUuid } =
          applicationsData.body.find(
            (application) => application.userUuid === uuid
          ) || {};
        const { paymentAmount, paymentMethod } =
          paymentsData.body.find(
            (payment) => payment.applicationUuid === applicationUuid
          ) || {};

        // Format table data to be passed into the table component, pay button tacked
        // onto the end to allow payments to be issued for each row
        return {
          uuid,
          name,
          email,
          requestedAmount: formatCurrency(requestedAmount),
          paymentAmount: formatCurrency(paymentAmount),
          paymentMethod,
          initiatePayment:
            !paymentAmount && requestedAmount ? (
              <Button
                onClick={() =>
                  initiatePayment({
                    applicationUuid,
                    requestedAmount,
                  })
                }
                variant="contained"
              >
                Pay
              </Button>
            ) : null,
        };
      })
    );
    setDataLoaded(true);
  }

  useEffect(() => fetchData(0, 10), []);

  const initiatePayment = async ({ applicationUuid, requestedAmount }) => {
    const { body } = await createPayment({
      applicationUuid,
      requestedAmount,
    });
    setPayments([...payments, body]);
  };

  const updatePagination = async (page, rowsPerPage) => {
    setPaginationCount(rowsPerPage);
    setPaginationPage(page);
    fetchData(page * rowsPerPage, rowsPerPage);
    console.log({ page, rowsPerPage });
  };

  return (
    <div className="App">
      <Container>
        {dataLoaded && (
          <Table
            data={tableData}
            userCount={userCount}
            paginationPage={paginationPage}
            paginationCount={paginationCount}
            updatePagination={updatePagination}
          />
        )}
      </Container>
    </div>
  );
};

export default App;
