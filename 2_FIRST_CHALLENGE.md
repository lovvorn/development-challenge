
# First Challenge
Welcome adventurer...

A student success manager (customer support) is having some trouble with the dashboard. They tell you that they clicked the pay button for `Kay Parsisson` and it didn't work. When the student success manager clicks the pay button for them it pays many more students than just them! 😱 Please fix the issue so that the student success manager can keep issuing payments confidently.


# Notes
1. Clicking "Pay" for Kay Parsisson has the effect described
2. Kay Parsisson has no "Requested Amount"
3. Clicking "Pay" on an aplication that has no Requested Amount marks all such applications as "Paid"
4. After analyzing the codem the issue is that users with no applications have a "Pay" button. They should not (as there is nothing to pay)

# Solutions
1. Front-end: Hide "Pay" button if "Requested Amount" is empty
2. Back-end: Ensure that commands to pay has an application ID and payment amount. If either are missing, do nothing and return
